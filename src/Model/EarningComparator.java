package Model;

import java.util.Comparator;

public class EarningComparator implements Comparator<Company> {

	@Override
	public int compare(Company o1, Company o2) {
		// TODO Auto-generated method stub
		if (o1.getIncome() < o2.getIncome()) {
			return -1;
		} else if (o1.getIncome() > o2.getIncome()) {
			return 1;
		} else {
			return 0;
		}
	}

	public String toString(){
		return "Name: ";
	}
}
