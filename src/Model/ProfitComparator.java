package Model;

import java.util.Comparator;

public class ProfitComparator implements Comparator<Company>{

	@Override
	public int compare(Company o1, Company o2) {
		// TODO Auto-generated method stub
		double com1 = o1.getIncome() - o1.getExpenses();
		double com2 = o2.getIncome() - o2.getExpenses();
		if (com1 < com2) {
			return -1;
		} else if (com1 > com2) {
			return 1;
		} else {
			return 0;
		}
	}

}
