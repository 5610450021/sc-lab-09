package Model;

import java.util.Comparator;

public class ExpenseComparator implements Comparator<Company>{

	@Override
	public int compare(Company o1, Company o2) {
		// TODO Auto-generated method stub
		if (o1.getExpenses() < o2.getExpenses()) {
			return -1;
		} else if (o1.getExpenses() > o2.getExpenses()) {
			return 1;
		} else {
			return 0;
		}
	}

}
