package Model;

import java.util.ArrayList;
import java.util.List;

import Interface.Traversal;

public class PreOrderTraversal implements Traversal{
	
	@Override
	public List<Node> traverse(Node node) {
		// TODO Auto-generated method stub
		List<Node> listnode = new ArrayList<Node>();
		if (node != null) {
		
			listnode.add(node);
			traverse(node.getLeft());
			traverse(node.getRight());
			
			
		}
		return listnode;
	}

}
