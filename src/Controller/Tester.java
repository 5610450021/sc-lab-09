package Controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import Interface.Measurable;
import Interface.Taxable;
import Model.BankAccount;
import Model.Company;
import Model.Country;
import Model.Data;
import Model.EarningComparator;
import Model.ExpenseComparator;
import Model.Person;
import Model.Product;
import Model.ProfitComparator;
import Model.TaxCalculator;
import Model.TaxComparator;

public class Tester {

	public static void main(String[] args) {
		Tester tester = new Tester();
//		tester.testPerson();
//		tester.testMin();
//		tester.testTax();
		tester.testCompare();
	}

	public void testPerson() {
		Measurable[] persons = new Measurable[3];
		persons[0] = new Person("Rick", 185);
		persons[1] = new Person("Glenn", 180);
		persons[2] = new Person("Carl", 175);
		double averageHeight = Data.average(persons);

		System.out.println("Test Person");
		System.out.println("Average height: " + averageHeight);
		System.out.println("Expected: 180.0");
		System.out.println("-----------------------------------");
	}

	public void testMin() {
		Measurable[] persons = new Measurable[2];
		persons[0] = new Person("Rick", 185);
		persons[1] = new Person("Glenn", 180);
		
		Measurable[] bankAccounts = new Measurable[2];
		bankAccounts[0] = new BankAccount("Rick", 2000);
		bankAccounts[1] = new BankAccount("Glenn", 4000);
		
		Measurable[] countries = new Measurable[2];
		countries[0] = new Country("USA", 318900000);
		countries[1] = new Country("Thailand", 67000000);
		
		System.out.println("Test Min");
		Person person = (Person) Data.min(persons[0], persons[1]);
		System.out.println("1. "+((Person) persons[0]).getName() + " " + persons[0].getMeasure());
		System.out.println("2. "+((Person) persons[1]).getName() + " " + persons[1].getMeasure());
		System.out.println("Min height: " + person.getName()+" "+person.getHeight() + "\n");
		
		BankAccount bankAccount = (BankAccount) Data.min(bankAccounts[0], bankAccounts[1]);
		System.out.println("1. "+((BankAccount) bankAccounts[0]).getName() + " " + bankAccounts[0].getMeasure());
		System.out.println("2. "+((BankAccount) bankAccounts[1]).getName() + " " + bankAccounts[1].getMeasure());
		System.out.println("Min balance: " + bankAccount.getName()+" "+bankAccount.getBalance() + "\n");
		
		Country country = (Country) Data.min(countries[0], countries[1]);
		System.out.println("1. "+((Country) countries[0]).getName() + " " + countries[0].getMeasure());
		System.out.println("2. "+((Country) countries[1]).getName() + " " + countries[1].getMeasure());
		System.out.println("Min population: " + country.getName()+" "+country.getPopulation());
		System.out.println("-----------------------------------");
	}
	public void testTax(){
		ArrayList<Taxable> persons = new ArrayList<Taxable>();
		persons.add(new Person("Rick", 185, 100000));
		persons.add(new Person("Glenn", 180, 500000));
		
		ArrayList<Taxable> companies = new ArrayList<Taxable>();
		companies.add(new Company("Apple", 1000000, 800000));
		companies.add(new Company("Windows", 2000000, 500000));
		
		ArrayList<Taxable> products = new ArrayList<Taxable>();
		products.add(new Product("TV", 10000));
		products.add(new Product("Computer", 20000));
		
		ArrayList<Taxable> allElements = new ArrayList<Taxable>();
		allElements.addAll(persons);
		allElements.addAll(companies);
		allElements.addAll(products);
		
		System.out.println("Test Tax");
		
		for(Taxable person:persons){
			Person p = (Person)person;
			System.out.println(p.getName()+" income:"+p.getYearlyIncome());
		}
		System.out.println("Tax sum persons: "+ TaxCalculator.sum(persons));
		System.out.println("Expected: 5000+35000=40000\n");
		
		for(Taxable company:companies){
			Company c = (Company) company;
			System.out.println(c.getName()+" income:"+c.getIncome()+" expenses:" + c.getExpenses());
		}
		System.out.println("Tax sum companies: "+ TaxCalculator.sum(companies));
		System.out.println("Expected: " + (200000*0.3)+"+"+(1500000*0.3) +" = "+510000+ "\n");
		
		for(Taxable product:products){
			Product p = (Product) product;
			System.out.println(p.getName()+" income:"+p.getPrice());
		}
		System.out.println("Tax sum products: "+ TaxCalculator.sum(products));
		System.out.println("Expected: 700+1400 = 2100 \n");
		
		System.out.println("Tax sum all elements: "+ TaxCalculator.sum(allElements));
		System.out.println("Expected: 40000+510000+2100 = 552100");
		System.out.println("-----------------------------------");
	}
	
	public void testCompare(){
		ArrayList<Person> persons = new ArrayList<Person>();
		persons.add(new Person("Monmon", 167, 100000));
		persons.add(new Person("May", 175, 10000));
		persons.add(new Person("Ployphuak", 160, 5000));
		persons.add(new Person("Kukkik", 200, 300000));
		
		Collections.sort(persons);
		System.out.println("Test Comparable:");
		System.out.println("Choice1:");
		for (Person person : persons) {
			System.out.println(person);
		}
		
		
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Product("TV", 30000));
		products.add(new Product("Macbook", 67500));
		products.add(new Product("IPhone", 37500));
		
		Collections.sort(products);
		System.out.println("-----------------------------------------------------");
		System.out.println("Choice2:");
		for (Product product : products) {
			System.out.println(product);
		}
		
		ArrayList<Company> company = new ArrayList<Company>();
		company.add(new Company("Monmon", 2500, 250));
		company.add(new Company("May", 1750, 750));
		company.add(new Company("Ployphuak", 1600, 500));
		company.add(new Company("Kukkik", 2000, 300));
		
		
		Collections.sort(company,new EarningComparator());
		System.out.println("-----------------------------------------------------");
		System.out.println("Choice3(Income):");
		for (Company comp : company) {
			System.out.println(comp);
		}
		
		Collections.sort(company,new ExpenseComparator());
		System.out.println("----------------------------");
		System.out.println("Choice3(Expense):");
		for (Company comp : company) {
			System.out.println(comp);
		}
		
		Collections.sort(company,new ProfitComparator());
		System.out.println("----------------------------");
		System.out.println("Choice3(Profit):");
		for (Company comp : company) {
			System.out.println(comp);
		}
		
		ArrayList<Taxable> taxall = new ArrayList<Taxable>();
		taxall.addAll(persons);
		taxall.addAll(products);
		taxall.addAll(company);
		
		Collections.sort(taxall,new TaxComparator());
		System.out.println("-----------------------------------------------------");
		System.out.println("Choice4:");
		for (Taxable taxable : taxall) {
			System.out.println(taxable.toString());
		}

	}
}
