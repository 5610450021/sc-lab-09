package Controller;

import java.util.List;

import Interface.Traversal;
import Model.InOrderTraversal;
import Model.Node;
import Model.PostOrderTraversal;
import Model.PreOrderTraversal;

public class ReportConsole {
	
	public void display(Node root,Traversal traversal){

		if (traversal.getClass() == InOrderTraversal.class) {
			System.out.println("Traverse with InorderTraversal : ");
//			for (Node nodes : traversal.traverse(root)) {
//				System.out.print(nodes.getValue() + " ");
//			}
		}
		else if (traversal.getClass() == PreOrderTraversal.class) {
			System.out.println("Traverse with PreorderTraversal : ");
//			for (Node nodes : traversal.traverse(root)) {
//				System.out.print(nodes.getValue() + " ");
//			}
		}
		else if (traversal.getClass() == PostOrderTraversal.class) {
			System.out.println("Traverse with PostorderTraversal : ");
//			for (Node nodes : traversal.traverse(root)) {
//				System.out.print(nodes.getValue() + " ");
//			}
		}
	
		System.out.println();
	}

	


}
