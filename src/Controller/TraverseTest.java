package Controller;

import Model.InOrderTraversal;
import Model.Node;
import Model.PostOrderTraversal;
import Model.PreOrderTraversal;


public class TraverseTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PreOrderTraversal preorder = new PreOrderTraversal();
		InOrderTraversal inorder = new InOrderTraversal();
		PostOrderTraversal postorder = new PostOrderTraversal();
		ReportConsole reportcon = new ReportConsole();	
		Node anode = new Node("A",null,null);
		Node cnode = new Node("C",null,null);
		Node enode = new Node("E",null,null);
		Node hnode = new Node("H",null,null);
		
		Node inode = new Node("I",hnode,null);
		Node gnode = new Node("G",null,inode);
		
		Node dnode = new Node("D",cnode,enode);
		Node bnode = new Node("B",anode,dnode);
		Node fnode = new Node("F",bnode,gnode);

		reportcon.display(fnode, preorder);
		reportcon.display(fnode, inorder);
		reportcon.display(fnode, postorder);
	}

}
